<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cetakxls extends CI_Controller {

	function __construct() {
		parent:: __construct();
		// load model pengurus
		$this->load->model('Model_pengurus');
		//load library PHPExcel
        $this->load->library('Excel/PHPExcel');
		//agar pengguna dipaksa login
		if(!$this->session->userdata('logged_in')) {redirect ('login','refresh');}
	}

	function index() {
		$objPHPExcel = new PHPExcel();
	    $objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
	    $objget = $objPHPExcel->getActiveSheet();  //inisiasi get object
	    $objget->setTitle('Sample Sheet'); //sheet title
	    //Warna header tabel
	    $objget->getStyle('A1:G1')->applyFromArray(
	        array(
	            'fill' => array(
	                'type' => PHPExcel_Style_Fill::FILL_SOLID,
	                'color' => array('rgb' => '92d050')
	            ),
	            'font' => array(
	                'color' => array('rgb' => '000000')
	            )
	        )
	    );
	    //table header
	    $cols = array('A','B','C','D','E');	     
	    $val = array('ID','NAMA','GENDER','ALAMAT','GAJI');
	    
	    for ($a=0;$a<count($cols);$a++) {//style baris 1
		    //berikan judul kolom
		    $objset->setCellValue($cols[$a].'1', $val[$a]);
		    //setting lebar
		    $objPHPExcel->getActiveSheet()->getColumnDimension($cols[$a])->setWidth(25);
		    $style = array(
		        'alignment' => array(
		            'horizontal' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
		        )
		    );
		    $objPHPExcel->getActiveSheet()->getStyle($cols[$a].'1')->applyFromArray($style);
	    }
	    $baris  = 2; //isi data mulai baris ke 2

	    //ambil data dari database
		$data=$this->Model_pengurus->getAll();
		foreach ($data->result() as $row) {
			//pemanggilan sesuaikan dengan nama kolom tabel
            $objset->setCellValue("A".$baris, $row->id); 
            $objset->setCellValue("B".$baris, $row->nama);
            $objset->setCellValue("C".$baris, $row->gender); 
            $objset->setCellValue("D".$baris, $row->alamat); 
            $objset->setCellValue("E".$baris, $row->gaji); 

            //format cell number untuk gaji
            $objPHPExcel->getActiveSheet()->getStyle('E1:E'.$baris)->getNumberFormat()->setFormatCode('0');
            $baris++;
		}

		//nama sheet
		$objPHPExcel->getActiveSheet()->setTitle('Rekapitulasi');
 
 		//nama file
        $objPHPExcel->setActiveSheetIndex(0);  
        $filename = urlencode("Rekapdata.xls");
       
        //generate excel
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$filename.'"'); 
        header('Cache-Control: max-age=0'); 
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');

	}

}